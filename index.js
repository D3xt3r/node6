const express = require("express");
const app = express();

app.listen(3000, () => {
  console.log("Iniciando el servidor en el puerto 3000");
});

app.get("/", (req, res) => {
  res.send("This is vulnerable site!!");
});
